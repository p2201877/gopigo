import socket
import math
import time
from easygopigo3 import EasyGoPiGo3


gpg = EasyGoPiGo3()
gpg.set_speed(900)
print("Tension : ", gpg.volt())

gpg.close_eyes()
#gpg.set_eye_color((0,0,7))
gpg.open_eyes()

hote = "192.168.1.104"
port = 12345


def get_position(request) :
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hote, port))
    # print ("Connection on {}".format(port))
    s.send(request.encode())
    answer = ((s.recv(4096)).decode()).split(", ")
    s.close()
    return answer
    # print ("Close")

while(True) : 
    i = 0
    initialisationDone = False
    gpg.set_eye_color((0,0, 50 + i*10))
    target = get_position("GTP")
    target_x = float(target[0])
    target_y = float(target[1])
    print("Coordonnées Target : ", target)

    robot_depart = get_position("GBP")
    robot_depart_x = float(robot_depart[0])
    robot_depart_y = float(robot_depart[1])
    print("Coordonnées Robot au départ : ", robot_depart)

    """initialisation"""

    distance = math.sqrt((target_x - robot_depart_x)**2 + (target_y - robot_depart_y)**2)
    print("Distance de départ : ", distance)

    gpg.drive_cm(10, True)
    print("Le robot avance de 10 cm.")

    robot = get_position("GBP")
    robot_x = float(robot[0])
    robot_y = float(robot[1])
    print("Coordonnées Robot après initialisation : ", robot)

    distance = math.sqrt((target_x - robot_x)**2 + (target_y - robot_y)**2)
    print("Distance : ", distance)
    

    """while loop"""
    
    gpg.SetMotorDps(gpg.MotorLeft, 1000)
    gpg.SetMotorDps(gpg.MotorRight, 1000)

    while(distance >= 20):

        delta_x = robot_x - robot_depart_x
        delta_y = robot_y - robot_depart_y

        print("delta_x : ", delta_x)
        print("delta_y : ", delta_y)

        if(delta_x > 0) : 
            angle_alpha =(180/math.pi)*math.atan(delta_y/delta_x)
        elif(delta_x == 0) :
            angle_alpha = 90
        else :
            angle_alpha = 180 + (180/math.pi)*math.atan(delta_y/delta_x)

        print("Angle alpha : ", angle_alpha)

        delta_target_x = target_x - robot_x
        delta_target_y = target_y - robot_y

        print("delta_target_x : ", delta_target_x)
        print("delta_target_y : ", delta_target_y)

        if(delta_target_x >= 0) : 
            angle_beta = (180/math.pi)*math.atan(delta_target_y/delta_target_x)
        elif(delta_target_x == 0) :
            angle_beta = 90
        else :
            angle_beta = 180 +(180/math.pi)*math.atan(delta_target_y/delta_target_x)

        print("Angle beta : ", angle_beta)

        angle_rotation = angle_beta - angle_alpha
        print("angle_rotation : ", angle_rotation)

        
        if initialisationDone == True :
            gpg.orbit(angle_rotation, 1/angle_rotation)
        else :
            gpg.turn_degrees(angle_rotation, True)
            initialisationDone = True
            gpg.drive_cm(distance/2, True)
        print("Le robot tourne et avance")
                
    
        robot_depart = robot
        robot_depart_x = float(robot_depart[0])
        robot_depart_y = float(robot_depart[1])


        robot = get_position("GBP")
        robot_x = float(robot[0])
        robot_y = float(robot[1])
        print("Coordonnées Robot : ", robot)

        distance = math.sqrt((target_x - robot_x)**2 + (target_y - robot_y)**2)
        print("Nouvelle Distance : ", distance)
        print("\n\n")
        i = (i+1) % 3

        adversary = get_position("GBP")

    #gpg.close_eyes()
    gpg.stop()
