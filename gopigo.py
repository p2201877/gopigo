import socket
import math
import time
import sys
from easygopigo3 import EasyGoPiGo3

couleur = sys.argv[1]
print (couleur)

gpg = EasyGoPiGo3()
gpg.set_speed(900)
print("Tension : ", gpg.volt())



hote = "192.168.1.10"
port = 12345


def get_position(request) :
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((hote, port))
    # print ("Connection on {}".format(port))
    s.send(request.encode())
    answer = ((s.recv(4096)).decode()).split(", ")
    s.close()
    return answer
    # print ("Close")

def AdversaryColor(string) :
    if string == 'GBP' :
        string = "GGP"
    elif string == "GGP" :
        string = "GBP"
    return string

target = get_position("GTP")
target_x = float(target[0])
target_y = float(target[1])
print("Coordonnées Target : ", target)

if couleur == 'g' :
    string = "GGP"
    gpg.set_eye_color((0 ,255, 0))
elif couleur == 'b' :
    string = "GBP"
    gpg.set_eye_color((0, 0, 255))

gpg.open_eyes()


robot_depart = get_position(string)
robot_depart_x = float(robot_depart[0])
robot_depart_y = float(robot_depart[1])
print("Coordonnées Robot au départ : ", robot_depart)

distance = math.sqrt((target_x - robot_depart_x)**2 + (target_y - robot_depart_y)**2)
print("Distance de départ : ", distance)

gpg.drive_cm(10, True)
print("Le robot avance de 10 cm.")

robot = get_position(string)
robot_x = float(robot[0])
robot_y = float(robot[1])
print("Coordonnées Robot après initialisation : ", robot)

while(True) : 
    i = 0
    target = get_position("GTP")
    target_x = float(target[0])
    target_y = float(target[1])
    print("Coordonnées Target : ", target)

    distance = math.sqrt((target_x - robot_x)**2 + (target_y - robot_y)**2)
    print("Distance de la target : ", distance)

    while(distance >= 20):

        delta_x = robot_x - robot_depart_x
        delta_y = robot_y - robot_depart_y

        print("delta_x : ", delta_x)
        print("delta_y : ", delta_y)

        if(delta_x > 0) : 
            angle_alpha =(180/math.pi)*math.atan(delta_y/delta_x)
        elif(delta_x == 0) :
            angle_alpha = 90
        else :
            if(delta_y <= 0) :
                angle_alpha = -180 + (180/math.pi)*math.atan(delta_y/delta_x)
            else :
                angle_alpha = 180 + (180/math.pi)*math.atan(delta_y/delta_x)

        print("Angle alpha : ", angle_alpha)

        delta_target_x = target_x - robot_x
        delta_target_y = target_y - robot_y

        print("delta_target_x : ", delta_target_x)
        print("delta_target_y : ", delta_target_y)

        if(delta_target_x > 0) : 
            angle_beta = (180/math.pi)*math.atan(delta_target_y/delta_target_x)
        elif(delta_target_x == 0) :
            angle_beta = 90
        else :
            if(delta_target_y <= 0) :
                angle_beta = -180 +(180/math.pi)*math.atan(delta_target_y/delta_target_x)
            else :
                angle_beta = 180 +(180/math.pi)*math.atan(delta_target_y/delta_target_x)

        print("Angle beta : ", angle_beta)
        if (((angle_alpha < 0) and (angle_beta > 0)) or ((angle_alpha > 0) and (angle_beta < 0))):
            if (angle_alpha < 0):
                angle_rotation = angle_beta - (360 + angle_alpha)
                print("angle_rotation : ", angle_rotation)
            else :
                angle_rotation = (360 + angle_beta) - angle_alpha
                print("angle_rotation : ", angle_rotation)

        gpg.turn_degrees(-angle_rotation, True)
        print("Le robot tourne de ", -angle_rotation)
        
        
        gpg.drive_cm((distance/2)/5, True)
        print("Le robot avance")
        
    
        robot_depart = robot
        robot_depart_x = float(robot_depart[0])
        robot_depart_y = float(robot_depart[1])

        """
        #Diviser en plusieurs gpg.drive_cm(5) et verifier la distance pour break, boucle for avec return
    
        for loop in range(3):
            gpg.drive_cm(10, True)

            robot = get_position("GBP")
            robot_x = float(robot[0])
            robot_y = float(robot[1])

            distance = math.sqrt((target_x - robot_x)**2 + (target_y - robot_y)**2)

            print(loop)

            if(distance <= 10) :
                #gpg.close_eyes()
                gpg.stop()
                exit()
        """

        adversary = get_position(AdversaryColor(string))
        adversary_x = float(adversary[0])
        adversary_y = float(adversary[1])

        delta_ad_x = robot_x - adversary_x
        delta_ad_y = robot_y - adversary_y

        if(delta_ad_x > 0) : 
            gpg.rotate_servo((180/math.pi) * math.atan(delta_ad_y/delta_ad_x))
        elif(delta_ad_x == 0) :
            gpg.rotate_servo(90)
        else :
            if(delta_y <= 0) :
                gpg.rotate_servo(20)
            else :
                gpg.rotate_servo(70)

    
        distance = math.sqrt((target_x - robot_x)**2 + (target_y - robot_y)**2)
        print("Nouvelle Distance : ", distance)
        print("\n\n")
    #gpg.close_eyes()
    gpg.stop()
